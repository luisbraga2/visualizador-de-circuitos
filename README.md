# **Visualizador de Circuitos**

#### O projeto busca desenvolver um visualizador de circuitos lógicos com interface agradável e que seja intuitivo para aplicações de aprendizado e construção de protótipos. O visualizador contará com uma boa faixa de diferentes portas lógicas, entradas(switchs, botões, relógios) e saidas (lampadas, osciloscopios), biestáveis e etc. O sistema será completamente reativo, ao ponto em que o usuário poderá ligar um botão a uma lampada e ver a mesma acender.
---

**VERSÃO:** 0.0.1.01

---
**COMO INICIAR O PROJETO:** pass

---
**AUTORES:** <br>
Gabriela Kaori @gkaori <br>
Victor Stevan @victorstevan <br>
Rafael Peixoto de Amorim @rafaelpxto <br>
Raquel Marques Ribeiro Oliveira @raeoliveira <br>
---
**INSTRUÇÕES PARA O USUÁRIO:** pass
